// import React, { useState } from "react";
import { useForm } from "react-hook-form";

export default function App() {
  // const [formFields, setFormFields] = useState({});

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    // setFormFields(data);
  };
  console.log(errors);
  // console.log(formFields);
  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    <div div className="container">
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* register your input into the hook by invoking the "register" function */}
        {/* <h2>{formFields.Firstname}</h2> */}

        <div className="form-group">
          <label>Name : </label>
          <input
            className="form-control"
            defaultValue=""
            {...register("Firstname", {
              required: "Please Enter your Name",
              maxLength: { value: 8, message: "Minimum length is 8" },
            })}
          />
          <br />
          <p className="text-danger"> {errors.Firstname?.message} </p>
          <br />
        </div>
        <div className="form-group">
          <label>Lastname : </label>
          {/* include validation with required or other standard HTML validation rules */}
          <input
            className="form-control"
            {...register("Lastname", {
              required: "Please Enter your lastname",
              maxLength: { value: 8, message: "Minimum length is 8" },
            })}
          />
          <br />
          {/* errors will return when field validation fails  */}
          <p className="text-danger"> {errors.Lastname?.message} </p>
          <br />
        </div>
        <div className="form-group">
          <label>Hobby:</label>
          <select
            className="form-control"
            {...register("gender", { required: "This is required" })}
          >
            <option value="Cricket">Cricket</option>
            <option value="Football">Football</option>
            <option value="Swimming">Swimming</option>
          </select>
        </div>
        <br />
        <br />
        <div className="form-group">
          <label>Email</label>
          <input
            className="form-control"
            {...register("Email", {
              required: "Email is required",
              pattern: {
                value: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/,
                message: "Please Enter valid Email",
              },
            })}
          />
          <br />
          <p className="text-danger"> {errors.Email?.message} </p>
          <br />
        </div>
        <div className="form-group">
          <label>Phone:</label>
          <input
            className="form-control"
            {...register("number", {
              required: "Number is required",
            })}
          />
          <br />
          <p className="text-danger"> {errors.number?.message} </p>
          <br />
        </div>
        <button className="btn btn-outline-primary" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}
